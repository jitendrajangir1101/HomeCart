from django.contrib import admin
from .models import *
admin.site.register(Product)
admin.site.register(ContactUs)
admin.site.register(Orders)

# Register your models here.
