from django.db import models
class Product(models.Model):
    product_id = models.AutoField
    product_name = models.CharField(max_length=50)
    category = models.CharField(max_length=400)
    subcategory = models.CharField(max_length=400)
    price = models.BigIntegerField()
    desc = models.CharField(max_length=500)
    pub_date = models.DateField()
    image = models.ImageField(upload_to="cart/images")
    def __str__(self):
        return self.product_name

# Create your models here.
class ContactUs(models.Model):
    msg_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    email= models.EmailField()
    desc = models.CharField(max_length=500)
    mobile=models.BigIntegerField()
    def __str__(self):
        return self.name
class Orders(models.Model):
    order_id=models.AutoField(primary_key=True)
    items_json=models.CharField(max_length=5000)
    name = models.CharField(max_length=500)
    email =models.EmailField()
    address =models.CharField(max_length=500)
    city =models.CharField(max_length=500)
    state = models.CharField(max_length=500)
    zip_code=models.BigIntegerField()
    phone=models.BigIntegerField(default=None)