from django.shortcuts import render
from .models import *
from math import ceil
def home(request):
    allProds = []
    catProds = Product.objects.values('category' ,'id')
    cats = {item['category'] for item in catProds}
    for cat in cats :
        prod = Product.objects.filter(category=cat)
        n=len(prod)
        nslides = n//4 + ceil((n/4)-(n//4))
        allProds.append([prod , range(1,nslides) , nslides])
    #params = {'no_of_slides':nslides ,'range':range(1 ,nslides) ,'product' : products}
    #allProds = [[products ,range(1 ,nslides) ,nslides ],[products ,range(1 ,nslides) ,nslides]]
    params={'allProds':allProds}
    return render (request , 'cart/home.html' , params)
def About(request):
    return render (request , 'cart/about.html')
def Contact(request):
    if request.method == 'POST':
        name=request.POST.get('name' , '')
        mobile=request.POST.get('mobile' , '')
        email=request.POST.get('email' , '')
        desc=request.POST.get('desc' , '')
        contact=ContactUs(name=name , email=email , mobile=mobile , desc=desc)
        contact.save()
        return render (request , 'cart/contact.html')
    else:
        return render (request , 'cart/contact.html')
def Tracker(request):
    return render (request , 'cart/tracker.html')
def ProductView(request , myid):
    product = Product.objects.filter(id=myid)
    return render (request , 'cart/productview.html' , {'product':product[0]})
def CheckOut(request):
    if request.method == 'POST':
        items_json=request.POST.get('itemsjson' , '')
        name=request.POST.get('name' , '')
        phone=request.POST.get('phone' , '')
        email=request.POST.get('email' , '')
        address=request.POST.get('address' , '')
        state=request.POST.get('state' , '')
        city=request.POST.get('city' , '')
        zip_code=request.POST.get('zip_code' , '')
        order=Orders(items_json=items_json, name=name , email=email , phone=phone , address=address ,state=state ,city=city ,zip_code=zip_code)
        order.save()
        thank = True
        id = order.order_id
        return render (request , 'cart/checkout.html', {'thank':thank, 'id': id})
    else:
        return render (request , 'cart/checkout.html')
def Search(request):
    return render (request , 'cart/search.html')

# Create your views here.
