
from django.urls import path
from . import views

urlpatterns = [
    path('' , views.home , name='home'),
    path('About/' , views.About , name='About'),
    path('Contact/' , views.Contact , name='Contact'),
    path('Tracker/' , views.Tracker , name='Tracker'),
    path('products/<int:myid>' , views.ProductView , name='ProductView'),
    path('CheckOut/' , views.CheckOut , name='CheckOut'),
    path('Search/' , views.Search , name='Search'),
    
]